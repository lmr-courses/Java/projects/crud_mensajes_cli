package com.monyba.mensajes.views;

import com.monyba.mensajes.models.MensajeModel;
import com.monyba.mensajes.services.MensajeService;

import java.util.Scanner;

public class MainView {
    public static void showMenu() {
        int response = 0;
        Scanner sc = new Scanner(System.in);

        do {
            System.out.println("=== Menu Principal ===");
            System.out.println("1. Crear Mensaje");
            System.out.println("2. Listar Mensajes");
            System.out.println("3. Obtener Mensaje");
            System.out.println("4. Editar Mensaje");
            System.out.println("5. Eliminar Mensaje");
            System.out.println("0. Salir");

            System.out.print("Ingrese Opcion: ");
            response = Integer.valueOf(sc.nextLine());

            switch (response){
                case 1: crearMensaje(sc); break;
                case 2: listarMensaje(); break;
                case 3: obtenerMensaje(sc); break;
                case 4: editarMensaje(sc); break;
                case 5: eliminarMensaje(sc); break;
                case 0: System.out.println("Thank you for you visit"); break;
                default: System.out.println("Please select a correct answer");
            }
        }while (response != 0);
    }

    public static void crearMensaje(Scanner sc){
        System.out.println("--- Crear Mensaje ---");
        System.out.print("Ingrese Mensaje: ");
        String mensaje = sc.nextLine();
        System.out.print("Ingrese Autor: ");
        String autor = sc.nextLine();
        MensajeModel mensajeModel = new MensajeModel(mensaje, autor);
        MensajeService.insertar(mensajeModel);
    }

    public static void listarMensaje(){
        System.out.println("--- Listar Mensajes ---");
        System.out.println("Id\tMensaje\tAutor\tFecha");
        for (MensajeModel m : MensajeService.listar()) {
            System.out.println(m.getId()+"\t"+m.getMensaje()+"\t"+m.getAutor()+"\t"+m.getFecha());
        }
    }

    public static void obtenerMensaje(Scanner sc){
        System.out.println("--- Obtener Mensajes ---");
        System.out.print("Ingrese Id: ");
        Integer id = Integer.parseInt(sc.nextLine());
        MensajeModel m = MensajeService.obtener(id);
        System.out.println("Id\tMensaje\tAutor\tFecha");
        System.out.println(m.getId()+"\t"+m.getMensaje()+"\t"+m.getAutor()+"\t"+m.getFecha());
    }

    public static void editarMensaje(Scanner sc){
        System.out.println("--- Editar Mensaje ---");
        System.out.print("Ingrese Id: ");
        Integer id = Integer.parseInt(sc.nextLine());
        System.out.print("Ingrese Mensaje: ");
        String mensaje = sc.nextLine();
        System.out.print("Ingrese Autor: ");
        String autor = sc.nextLine();
        MensajeModel mensajeModel = new MensajeModel(id, mensaje, autor);
        MensajeService.actualizar(mensajeModel);
    }

    public static void eliminarMensaje(Scanner sc){
        System.out.println("--- Eliminar Mensaje ---");
        System.out.print("Ingrese Id: ");
        Integer id = Integer.parseInt(sc.nextLine());
        MensajeService.eliminar(id);
    }
}
