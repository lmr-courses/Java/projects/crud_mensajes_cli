package com.monyba.mensajes.models;

import java.sql.Date;
import java.time.LocalDateTime;

public class MensajeModel {
    private Integer id;
    private String mensaje;
    private String autor;
    private LocalDateTime fecha;


    public MensajeModel() {
    }

    public MensajeModel(String mensaje, String autor) {
        this.mensaje = mensaje;
        this.autor = autor;
    }

    public MensajeModel(Integer id, String mensaje, String autor) {
        this.id = id;
        this.mensaje = mensaje;
        this.autor = autor;
    }

    public MensajeModel(Integer id, String mensaje, String autor, LocalDateTime fecha) {
        this.id = id;
        this.mensaje = mensaje;
        this.autor = autor;
        this.fecha = fecha;
    }

    public String getFechaAsString(){
        return "";
    }

    // Getters and Setters
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public LocalDateTime getFecha() {
        return fecha;
    }

    public void setFecha(LocalDateTime fecha) {
        this.fecha = fecha;
    }
}
