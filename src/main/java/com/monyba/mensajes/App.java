package com.monyba.mensajes;
import com.monyba.mensajes.configs.DataBaseConnection;

import java.sql.Connection;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.monyba.mensajes.views.MainView.*;
/**
 * Hello world!
 *
 */
public class App {
    private static final Logger logger = Logger.getLogger(App.class.getName());
    static String cantGetConnection = "\nNo se pudo establecer la conexión con la Base de Datos: ";

    public static void main( String[] args ) {
        logger.info("Wellcome to Mensajes App");
        if (args.length > 0) {
            logger.info(Arrays.toString(args));
        }

        DataBaseConnection.createTable();
        showMenu();

    }
}
