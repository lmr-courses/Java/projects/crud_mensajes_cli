package com.monyba.mensajes.daos;

import com.monyba.mensajes.configs.DataBaseConnection;
import com.monyba.mensajes.helpers.DateHelper;
import com.monyba.mensajes.models.MensajeModel;
import com.monyba.mensajes.services.MensajeService;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MensajeDao {
    private static final Logger logger = Logger.getLogger(MensajeDao.class.getName());

    static String cantGetConnection = "\nNo se pudo establecer la conexión con la Base de Datos: ";
    static String cantCreateMessage = "\nNo se pudo insertar el mensaje: ";
    static String cantListMessages = "\nNo se pudo listar los mensajes: ";
    static String cantGetMessages = "\nNo se pudo obtener el mensaje: ";
    static String cantUpdateMessages = "\nNo se pudo editar el mensaje: ";
    static String cantDeleteMessages = "\nNo se pudo eliminar el mensaje: ";
    static String cantGetInsertId = "\nNo se pudo obtener el Id: ";
    static String noAffectedRows = "\nNo hay filas afectadas: ";


    public static Long insertar(MensajeModel model){
        Long modelId = null;
        model.setFecha(LocalDateTime.now());
        try (Connection cnx = DataBaseConnection.getInstance()) {
            cnx.setAutoCommit(false);
            PreparedStatement ps;
            try {
                String qry="INSERT INTO mensajes(mensaje, autor, fecha) values (?, ?, ?)";

                ps=cnx.prepareStatement(qry, Statement.RETURN_GENERATED_KEYS);
                ps.setString(1, model.getMensaje());
                ps.setString(2, model.getAutor());
                ps.setTimestamp(3, DateHelper.getUTCDate(model.getFecha()));

                int affectedRows = ps.executeUpdate();
                if (affectedRows == 0) throw new SQLException(noAffectedRows);
                try (ResultSet generatedKeys = ps.getGeneratedKeys()) {
                    if(!generatedKeys.next()) throw new SQLException(cantGetInsertId);
                    modelId = generatedKeys.getLong(1);
                }

                ps.close();
                cnx.commit();
            } catch (SQLException e) {
                cnx.rollback();
                logger.log(Level.SEVERE,cantCreateMessage+e);
            } finally {
                cnx.close();
            }
        }catch(Exception ex){
            logger.log(Level.SEVERE,cantGetConnection+ex);
        }
        return modelId;
    }

    public static List<MensajeModel> listar(){
        List<MensajeModel> rsp = new ArrayList<>();
        try (Connection cnx = DataBaseConnection.getInstance()) {
            cnx.setAutoCommit(false);
            PreparedStatement ps;
            ResultSet rs;
            try {
                String qry="SELECT id, mensaje, autor, fecha FROM mensajes";

                ps=cnx.prepareStatement(qry);

                rs = ps.executeQuery();
                while(rs.next()){
                    rsp.add(new MensajeModel(rs.getInt("id"),rs.getString("mensaje"),rs.getString("autor"),rs.getTimestamp("fecha").toLocalDateTime()));
                }

                rs.close();
                ps.close();
            } catch (SQLException e) {
                logger.log(Level.SEVERE,cantListMessages+e);
            } finally {
                cnx.close();
            }
        }catch(Exception ex){
            logger.log(Level.SEVERE,cantGetConnection+ex);
        }
        return rsp;
    }

    public static MensajeModel obtener(Integer id){
        MensajeModel rsp = null;
        try (Connection cnx = DataBaseConnection.getInstance()) {
            cnx.setAutoCommit(false);
            PreparedStatement ps;
            ResultSet rs;
            try {
                String qry="SELECT id, mensaje, autor, fecha FROM mensajes WHERE id=?";

                ps=cnx.prepareStatement(qry);
                ps.setInt(1, id);

                rs = ps.executeQuery();
                if(rs.next()){
                    rsp = new MensajeModel(rs.getInt("id"),rs.getString("mensaje"),rs.getString("autor"),rs.getTimestamp("fecha").toLocalDateTime());
                }

                rs.close();
                ps.close();
            } catch (SQLException e) {
                logger.log(Level.SEVERE,cantGetMessages+e);
            } finally {
                cnx.close();
            }
        }catch(Exception ex){
            logger.log(Level.SEVERE,cantGetMessages+ex);
        }
        return rsp;
    }

    public static Integer editar(MensajeModel model){
        Integer affectedRows = null;
        model.setFecha(LocalDateTime.now());
        try (Connection cnx = DataBaseConnection.getInstance()) {
            cnx.setAutoCommit(false);
            PreparedStatement ps;
            try {
                String qry="UPDATE mensajes SET mensaje=?, autor=?, fecha=? WHERE id=?";

                ps=cnx.prepareStatement(qry);
                ps.setString(1, model.getMensaje());
                ps.setString(2, model.getAutor());
                ps.setTimestamp(3, DateHelper.getUTCDate(model.getFecha()));
                ps.setInt(4, model.getId());

                affectedRows = ps.executeUpdate();
                if (affectedRows == 0) throw new SQLException(noAffectedRows);

                ps.close();
                cnx.commit();
            } catch (SQLException e) {
                cnx.rollback();
                logger.log(Level.SEVERE,cantUpdateMessages+e);
            } finally {
                cnx.close();
            }
        }catch(Exception ex){
            logger.log(Level.SEVERE,cantGetConnection+ex);
        }
        return affectedRows;
    }

    public static Integer eliminar(Integer id){
        Integer affectedRows = null;
        try (Connection cnx = DataBaseConnection.getInstance()) {
            cnx.setAutoCommit(false);
            PreparedStatement ps = null;
            try {
                String qry="DELETE FROM mensajes WHERE id=?";

                ps=cnx.prepareStatement(qry);
                ps.setInt(1, id);

                affectedRows = ps.executeUpdate();
                if (affectedRows == 0) throw new SQLException(noAffectedRows);

                ps.close();
                cnx.commit();
            } catch (SQLException e) {
                logger.log(Level.SEVERE,cantDeleteMessages+e);
            } finally {
                cnx.close();
            }
        }catch(Exception ex){
            logger.log(Level.SEVERE,cantGetConnection+ex);
        }
        return affectedRows;
    }
}
