package com.monyba.mensajes.services;

import com.monyba.mensajes.daos.MensajeDao;
import com.monyba.mensajes.models.MensajeModel;

import java.util.List;
import java.util.logging.Logger;

public class MensajeService {
    private static final Logger logger = Logger.getLogger(MensajeService.class.getName());

    public static void insertar(MensajeModel model){
        Long result = MensajeDao.insertar(model);
        logger.info("insertar: "+result);
    }

    public static List<MensajeModel> listar(){
        return MensajeDao.listar();
    }

    public static MensajeModel obtener(Integer id){
        return MensajeDao.obtener(id);
    }

    public static void actualizar(MensajeModel model){
        Integer result = MensajeDao.editar(model);
        logger.info("actualizar: "+result);
    }

    public static void eliminar(Integer id){
        Integer result = MensajeDao.eliminar(id);
        logger.info("eliminar: "+result);
    }
}
