package com.monyba.mensajes.helpers;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import static java.time.format.DateTimeFormatter.ISO_LOCAL_DATE_TIME;

public class DateHelper {
    public static Timestamp getUTCDate(LocalDateTime dateTime){
        // Setting Local Zone
        ZonedDateTime zonedLocal = dateTime.atZone(ZoneId.of("America/Lima"));
        // Convert to UTC
        ZonedDateTime zonedUTC = zonedLocal.withZoneSameInstant(ZoneId.of("UTC"));
        // Return
        return Timestamp.valueOf(zonedUTC.toLocalDateTime());
    }
}
