package com.monyba.mensajes.configs;

import com.monyba.mensajes.daos.MensajeDao;
import com.monyba.mensajes.helpers.DateHelper;
import io.github.cdimascio.dotenv.Dotenv;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DataBaseConnection {
    static String cantGetConnection = "\nNo se pudo establecer la conexión con la Base de Datos: ";
    private static final Logger logger = Logger.getLogger(DataBaseConnection.class.getName());
    static String noAffectedRows = "\nNo hay filas afectadas: ";
    static String cantCreateTable = "\nNo se pudo crear la tabla: ";
    private static Connection connection;

    private DataBaseConnection(){}

    public static Connection getInstance() throws SQLException {

        if (connection == null || connection.isClosed()) {
            Dotenv dotenv = Dotenv.load();
            String host = dotenv.get("DATABASE_HOST");
            String port = dotenv.get("DATABASE_PORT");
            String name = dotenv.get("DATABASE_NAME");
            String user = dotenv.get("DATABASE_USER");
            String pass = dotenv.get("DATABASE_PASSWORD");
            String url = "jdbc:postgresql://"+host+":"+port+"/"+name;

            connection = DriverManager.getConnection(url, user, pass);
        }

        return connection;
    }

    public static void createTable(){
        try (Connection cnx = getInstance()) {
            PreparedStatement ps;
            try {
                String qry="DROP TABLE IF EXISTS mensajes";
                ps=cnx.prepareStatement(qry);
                ps.executeUpdate();

                qry="CREATE TABLE IF NOT EXISTS mensajes ("+
                    "id SERIAL NOT NULL,"+
                    "mensaje VARCHAR(300) NOT NULL,"+
                    "autor VARCHAR(100) NOT NULL,"+
                    "fecha TIMESTAMP NOT NULL,"+
                    "CONSTRAINT mensaje_pk PRIMARY KEY (id)"+
                    ")";

                ps=cnx.prepareStatement(qry);
                int affectedRows = ps.executeUpdate();
                if (affectedRows == 0) throw new SQLException(noAffectedRows);

                ps.close();
                cnx.commit();
            } catch (SQLException e) {
                cnx.rollback();
                logger.log(Level.SEVERE,cantCreateTable+e);
            } finally {
                cnx.close();
            }
        }catch(Exception ex){
            logger.log(Level.SEVERE,cantGetConnection+ex);
        }
    }

}
