Esta es una Aplicacion Simple de Java CLI CRUD con Acceso a Base de Datos.

Esta Aplicacion fue construida con Maven usando el arquetipo: maven-archetype-quickstart

Se le agrego Maven Wrapper con: mvn wrapper:wrapper

Se puede ejecutar usando: ./mvnw compile exec:java -Dexec.mainClass="com.monyba.mensajes.App" -Dexec.arguments="Hello,World"

Se agrego y configuro (exec-maven-plugin) un pluging en el POM para hacerlo mas simple usando : ./mvnw compile exec:java

Otros Comandos:  
./mvnw clean package  
./mvnw clean install  
./mvnw spring-boot:run

La Base de Datos:

CREATE DATABASE mensajes_app;

DROP TABLE IF EXISTS mensajes;
CREATE TABLE IF NOT EXISTS mensajes (
id SERIAL NOT NULL,
mensaje VARCHAR(300) NOT NULL,
autor VARCHAR(100) NOT NULL,
fecha TIMESTAMP NOT NULL,
CONSTRAINT mensaje_pk PRIMARY KEY (id)
);